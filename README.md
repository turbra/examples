Sample examples to use
---

[toc]

# Job Templates:

If you want to create a template job that can be extended by other jobs, you can prefix the template job name with a `.` to indicate that it's a hidden job (which won't run by itself). Jobs that want to use this template can then utilize the `extends` keyword.

Here's how you can accomplish this:

`.gitlab-ci.yml`:

```yaml
# This is the template job
.build_docker_template:
  stage: build
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME .
  tags:
    - $RUNNER_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == $TARGET_BRANCH'

# Other jobs can extend this template
build_app_image:
  extends: .build_docker_template
  # Any other custom configurations or overrides specific to "example_job"
```

In this configuration:

- `.build_docker_template`: This is your template. It's prefixed with a dot, meaning it's hidden and won't run by itself. 
- `example_job`: This is an example of a job that extends the `.build_docker_template`. This job inherits everything from the template but can also have its own configurations or even override settings from the template.

You can add as many jobs as needed, and each can use `extends: .build_docker_template` to inherit the configurations from the template.

---

# Job Templates Directory:
If you'd like to keep your `.build_docker_template` (or any CI/CD related template) in a separate YAML file under a directory named `scripts` within your repository, you can use GitLab CI's `include` feature to include external files.

Here's how you can achieve this:

1. **Move Template to External File**:
   
   Create a new YAML file within your repository under `scripts` directory named, let's say, `docker_template.yml`:

**`scripts/docker_template.yml`**:

```yaml
.build_docker_template:
  stage: build
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME .
  tags:
    - $RUNNER_TAG
  rules:
    - if: '$CI_COMMIT_BRANCH == $TARGET_BRANCH'
```

2. **Include External Template in Main CI File**:

   In your main `.gitlab-ci.yml` file, you will `include` this external file and then define jobs that extend from it:

**`.gitlab-ci.yml`**:

   ```yaml
   include:
     - scripts/docker_template.yml

   build_app_image:
     extends: .build_docker_template
     # Any other custom configurations or overrides specific to "build_app_image"
   ```

Using this approach, your CI configurations become modular, and it's easier to manage especially when you have multiple templates or shared configurations across multiple projects or jobs.

---

# Branch-Specific CI/CD Jobs:
Below is an example where jobs are configured to run only on specific branches using the `rules` keyword for job-level configurations in the `.gitlab-ci.yml` file.

## Configuration Example:

```yaml
stages:
  - build
  - deploy

# Job that runs only on 'main' branch
build_main:
  stage: build
  script:
    - echo "Building for main branch"
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'

# Job that runs only on 'develop' branch
build_develop:
  stage: build
  script:
    - echo "Building for develop branch"
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'

# Job that runs on all branches except 'main'
deploy_except_main:
  stage: deploy
  script:
    - echo "Deploying for all branches except main"
  rules:
    - if: '$CI_COMMIT_BRANCH != "main"'
```

## Explanation:

- `build_main`: This job will only run when changes are pushed to the `main` branch.
- `build_develop`: This job will only run when changes are pushed to the `develop` branch.
- `deploy_except_main`: This job will run on changes to any branch except the `main` branch.

## Notes:

The `rules` keyword at the job level is used to specify conditions for when the job should run. Each `rule` is evaluated in order and the job is run if the `if` condition in the `rule` is met. If no `rule` is met, the job is skipped.

---

# Feature Branch Testing:
   
   Jobs designed to run tests on feature branches which are commonly prefixed with `feature/`.

```yaml
test_feature_branches:
  script:
    - echo "Running tests for feature branch"
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^feature\//'
```

# Tag-based Deployments:

   Instead of branches, sometimes deployments are triggered based on tags, especially for releases.

```yaml
deploy_release:
  script:
    - echo "Deploying release"
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+/'
      when: always
```

# Jobs Specific to Merge Requests:

   For actions you want to take only when a merge request is created.

```yaml
review_app:
  script:
    - echo "Setting up a review app"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```

# Environment-specific Deployments:

   Deploy to staging and production based on **environment** definition.
```yaml
deploy_prod:
  stage: deploy
  script:
    - echo "Deploy to production server"
  environment:
    name: production
    url: https://example.com
  when: manual
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

   Deploy to staging and production based on **branch** names.

```yaml
deploy_staging:
  script:
    - echo "Deploying to staging"
  rules:
    - if: '$CI_COMMIT_BRANCH == "develop"'

deploy_production:
  script:
    - echo "Deploying to production"
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
```

# Scheduler Jobs:

   Some jobs might run only when triggered by a scheduler in GitLab.

```yaml
nightly_build:
  script:
    - echo "Running nightly build"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
```
# Use envsubst:
  envsubst substitutes the value of environment variables in your k8s deployment specs.

  In your `deployment.yml`:
   ```yaml
   spec:
     containers:
     - name: my-container
       image: my-repo/my-image:${IMAGE_TAG}
   ```

   In your `.gitlab-ci.yml`'s deploy script:
   ```bash
   - export IMAGE_TAG=$CI_COMMIT_SHORT_SHA
   - envsubst < deployment.yml | oc apply -f -
   ```
